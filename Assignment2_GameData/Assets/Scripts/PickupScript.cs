﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{
    // Start is called before the first frame update

    //For the purpose of this assignment, my Pickups will have the following attributes.
    //Change in Scale
    //Change in Position
    //Change in Color

    public Color pickupColor;
    public Transform pickupTransform;

    void Start()
    {
        pickupColor = transform.GetChild(0).GetComponent<SpriteRenderer>().color;
        pickupTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(0.1f + Mathf.PingPong(Time.time * 1f, 1),1,1);
        pickupColor = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time, 1));
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = pickupColor;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z+1);
    }
}
