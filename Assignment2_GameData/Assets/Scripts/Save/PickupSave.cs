﻿using System;
using UnityEngine;

[Serializable]
public class PickupSave : Save
{
    //Data is a filke type that we will be using for the save files
    public Data data;
    private PickupScript pickup;
    //the json string is the name of the save file
    private string jsonString;

    [Serializable]
    public class Data : BaseData
    {
        //The save data we need to store
        public Vector4 pickupColor;
        public Vector3 pickupLocation;
        public Vector3 pickupEulerAngle;
        public Vector3 pickupScale;


    }

    void Awake()
    {
        pickup = GetComponent<PickupScript>();
        data = new Data();
    }

    public override string Serialize()
    {
        //Serializing data means to take it apart and break it into its most basic parts
        data.prefabName = prefabName;
       data.pickupColor = transform.GetChild(0).GetComponent<SpriteRenderer>().color;
        data.pickupLocation = transform.position;
        data.pickupScale = transform.localScale;
        data.pickupEulerAngle = transform.eulerAngles;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData)
    {
        //Deserializing the data means to take the broken down basic data and recreate the gameobject from it
        JsonUtility.FromJsonOverwrite(jsonData, data);
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = data.pickupColor;
        transform.position= data.pickupLocation;
        transform.localScale = data.pickupScale;
        transform.eulerAngles = data.pickupEulerAngle;
        pickup.name = "Pickup";
    }
}