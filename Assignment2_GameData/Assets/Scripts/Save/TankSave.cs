﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {
    //Data is a filke type that we will be using for the save files
    public Data data;
    private Tank tank;
    //the json string is the name of the save file
    private string jsonString;

    [Serializable]
    public class Data : BaseData {
        //The save data we need to store
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    public override string Serialize() {
        //Serializing data means to take it apart and break it into its most basic parts
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData) {
        //Deserializing the data means to take the broken down basic data and recreate the gameobject from it
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}